# scripts

This repository contains scripts used for maintaining Venom Linux, including release engineering.
All scripts are written in posix sh and most of them have names describing their function.

## build.sh
`build.sh` is the most complex script of the repository. It is used for operations on a rootfs, such as building packages and creating updated rootfs tarbals and isos.
With `./buid.sh -h` one can see its option and basic usage. It has minimal requirements and if called with an argument that will require something not available in the system it will inform the user.
It is used by other wrapper scripts in the repo to accomplish specific tasks:

- `makeiso-base`: Creates a base iso, serves as a building block for `makeiso.sh`, `makeiso-runit` and `makeiso-wl` that add more packages and configuration.
- `makerootfs.sh`:	Creates an updated rootfs tarball.
- `portupdate.sh`: Updates ports in a chroot.

